// Опишіть, як можна створити новий HTML тег на сторінці.

// Щоб створити новий тег на сторінці потрібно використати метод insertAdjacentHTML(a, b),
// де а - місце вставки, b - сам елемент.
// Наприклад, так ми додамо елемент <div> одразу після обраного елемента element:
// element.insertAdjacentHTML("afterend", "<div>");.
// Можемо параметру b надати значення, наприклад, так:
// let newElement = "<div>Hello!</div>";
// element.insertAdjacentHTML("afterend", newElement);
// Також можна працювати з вузлами:
// - створити вузол, використовуючи метод createElement();
// - додати на сторінку за допомогою методу вставки вузла. В нашому випадку:
// element.after(document.createElement("div"));.
// На сторінці під вузлом з'явиться: <div></div>.


// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Перший параметр функції визначає місце розташування елемента HTML, що створюється.
// варіанти:
// - "beforebegin" - перед елементом;
// - "afterbegin" - всередині елемента на його початку;
// - "beforeend" - всередині елемента на його кінці;
// - "afterend" - одразу після елемента.


// Як можна видалити елемент зі сторінки?
// Метод removeChild дозволяє видалити елемент.Застосовується до батьківського елемента із зазначенням елемента, який потрібно видалити.
// батько.removeChild(елемент); (https://code.mu/ru/javascript/manual/dom/removeChild/)



// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, 
// до якого буде прикріплений список (по дефолту має бути document.body).
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

let array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let element;
displayListOnPage(array);
// parentElement это родительский элемент текущего узла. Это всегда объект DOM Element , или null 

function displayListOnPage (array, parent = document.body) {
	// Метод createElement позволяет создать новый элемент, передав в параметре имя тега
	let ul = document.createElement('ul');

	parent.prepend(ul);
	for (let i of array) {
		let Li = `<li>${i}</li>`;
		ul.insertAdjacentHTML("beforeend", Li);
	 }	
}


// second way
let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
html = '<ul>';
arr.forEach(function(item) {
  html += '<li>'+item+'</li>';
});
html += '</ul>'

document.write(html);


// не встигла до дідлайну зробити додаткові завдання:
// Додайте обробку вкладених масивів.
// Якщо всередині масиву одним із елементів буде ще один масив,виводити його як вкладений список.
// Приклад такого масиву:
// ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

// Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.

// Очистити сторінку через 3 секунди.
// Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.